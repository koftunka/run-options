# Extract node run command option arguments #



### A simple npm module to extract node command line option argumets ###

* Has a default set of options
* Ability to override/add option maps by specifying a project config file
* Support for -- and - options, boolean flags
* List all available option with --help
* Verbose output with --verbose

### Use case ###

```bash
node ./my_project/script.js --path /app/build -a update
```

in script.js:
```script
let options = require('run-options')();

console.log(options);
/*
{ 
  path: '/app/build',
  action: 'update',
  pattern: 'pattern',
  version: 'latest'
  verbose: false 
}
*/
```

You can see that there are additional options 'pattern', 'version', 'verbose'. 
Those fields are populated by default option settings.

### Available default options ###

```bash
{ path: './',
  action: 'update',
  pattern: 'pattern',
  force: false,
  version: 'latest',
  port: 8080,
  verbose: true }
```

### Listing all available options ###

```bash 
node ./my_project/script.js --help
```

### Verbose output ###

Will echo options/errors

```bash 
node ./my_project/script.js --verbose
```

### Configuration ###

* create /lib/arguments.js file in the root of your project



#### Add option/boolean: ####
```bash
//  /lib/arguments.js
module.exports = [
        // add option
        {
		"full" : "myoption", // --myoption
		"short" : "mo",   // -mo
		"default" : "default value for myoptions",
		"description" : "Description for myoption to be used by --help"
	}, 
        // add boolean flag option
         {
		"full" : "myflag", // --myflag
		"short" : "mf",   // -mf
            "bool" : true, // add bool property
		"default" : false,
		"description" : "Description for myflag"
	}
];
```