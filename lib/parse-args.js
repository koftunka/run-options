"use strict";
function parseArgs() {
    var path = require('path'),
        customArgsMap = [],
        argsMap = [],
        options = {},
        args = process.argv,
        dashes = /-/g,
        errors = [];

    try {
        customArgsMap = require(path.resolve(__dirname, '../../../lib/arguments.js'));
    } catch (e) {
        errors.push(e);
    }

    try {
        argsMap = require('./arguments.js')
    } catch (e) {
        errors.push(e);
    }

    function ascii () {
        console.log("   ___              ____       __  _");
        console.log("  / _ \\__ _____    / __ \\___  / /_(_)__  ___  ___");
        console.log(" / , _/ // / _ \\  / /_/ / _ \\/ __/ / _ \\/ _ \\(_-<");
        console.log("/_/|_|\\_,_/_//_/  \\____/ .__/\\__/_/\\___/_//_/___/");
        console.log("                      /_/");
    }

    function showHelp() {
        ascii();
        argsMap.map(item => {
            item.short = item.short || ' no short version';
            console.log(`--${item.full} -${item.short} : ${item.description}, default value: ${item.default}`);
        });
    }

    if (argsMap && argsMap.length) {
        if (customArgsMap && customArgsMap.length) {
            customArgsMap.map(item => {
                let swapped;
                for (let i = 0, l = argsMap.length; i < l; i++) {
                    if ((argsMap[i].full && item.full && argsMap[i].full == item.full)
                      || (argsMap[i].short && item.short && argsMap[i].short == item.short)) {
                        argsMap[i] = item;
                        swapped = true;
                        break;
                    }
                }

                if (!swapped) {
                    argsMap.push(item);
                }
            });
        }

        // set defaults
        argsMap.map(item => {
            if (item && item.full && item.default !== void 0) {
                options[item.full] = item.default;
            }
        });
        // console.log('args', args);

        if (args && args.length) {
            args = args.slice(2);
            //console.log(args);

            args.map((key, index) => {
                var value,
                    config;

                if (key.charAt(0) != '-') {
                    return;
                }
                key = key.replace(dashes, '');

                config = argsMap.filter(item => {
                    if (item.full == key || item.short == key) {
                        key = item.full;
                        return item;
                    }
                });

                if (config && config[0]) {
                    config = config[0];
                    value = args[index + 1];

                    if (value && value.charAt(0) !== '-') {
                        options[key] = value;
                    } else if (config) {
                        if (config.bool) {
                            options[key] = true;
                        }
                    }
                }

                if (key == 'help') {
                    options.help = 1;
                    showHelp();
                }
            });
        }
    }
    if (options.verbose) {
        console.log('Options:\n', options);
        errors.map(error => console.log('${error}'));
    }
    return options;
}

module.exports = parseArgs;
