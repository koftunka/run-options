module.exports = [
    {
        "full" : "path", // --path
        "short" : "p",   // -p
        "default" : "./",
        "description" : "Location path"
    }, {
        "full" : "action",
        "short" : "a",
        "default" : "update",
        "description" : "Action to perform"
    }, {
        "full" : "pattern",
        "short" : "pattern",
        "default" : "pattern",
        "description" : "Pattern to match"
    }, {
        "full" : "force",
        "short" : "f",
        "default" : false,
        "bool" : true,
        "description" : "Boolean force flag"
    }, {
        "full" : "version",
        "short" : "v",
        "default" : "latest",
        "description" : "Version"
    }, {
        "full" : "port",
        "short" : "port",
        "default" : 8080,
        "description" : "Port number"
    }, {
        "full" : "verbose",
        "default" : false,
        "bool" : true,
        "description" : "Verbose execution"
    }
];